## São Paulo Life's Dashboard

# Introduction

The goal of my website is to provide São Paulo (Brazil) citizen’s a dashboard to relevant information to start their daily routines, including: traffic report, subway and train line status, major avenues real-time video, flood and weather reports, all on one single page. These information are scattered around several sites. It would be great to have all this information consolidated on one place.

Since São Paulo is the most populated city of the Southern Hemisphere, every information available could help citizens avoid traffic jams and other issues, mostly related to commuting.

This is still working in progress, but I preview version can be found here: https://db.eti.br/

# Expected List of Features

- Subway lines status: provide subway status / issues information, provided by http://www.viaquatro.com.br/
- Train line status: provide train status / issues information, provided by http://www.viaquatro.com.br/
- Bus lane status: provided by “Olho Vivo” (http://www.cetsp.com.br/)
- Traffic report: total jam provided by two sources (http://www.cetsp.com.br/) and (http://transito.maplink.global/SP/sao_paulo/TransitoAgora)
- Traffic visualization: provided by Waze Live Map (https://www.waze.com/livemap)
- Weather report: provided by https://openweathermap.org/api
- Flood report: provided by (https://www.cgesp.org/v3/alagamentos.jsp)

# Third party libraries: 
- jQuery
- Popper.js
- Bootstrap
- PHP Proxy from Iacovos Constantinou (https://github.com/softius)